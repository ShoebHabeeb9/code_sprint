**DATAMINE** \
***TIME SERIES ANALYSIS OF SWAN-SF DATA***

SWAN SF is a novel time series dataset of the solar activity on the surface of the sun. \
In this project i try to understand and build classical/deep learning  models which will make meaningful analysis of the dataset.

***Created by Shoeb Habeeb*** 